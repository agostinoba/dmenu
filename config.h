/* See LICENSE file for copyright and license details. */
/* Default settings; can be overriden by command line. */

static const char col_graybg[]  = "#222222";
static const char col_graybg2[] = "#2f2f2f";
static const char col_graybg3[] = "#444444";
static const char col_cyan[]    = "#005577";
static const char col_frost1[]  = "#8fbcbb"; // Nordtheme Frost
static const char col_frost2[]  = "#88c0d0"; // Nordtheme Frost
static const char col_frost3[]  = "#81a1c1"; // Nordtheme Frost
static const char col_frost4[]  = "#5e81ac"; // Nordtheme Frost

/* -b  option; if 0, dmenu appears at bottom     */
static int topbar = 1;

/* -fn option overrides fonts[0]; default X11 font or font set */
static const char *fonts[] = {
	"Iosevka Slab:size=9"
};

/* -p  option; prompt to the left of input field */
static const char *prompt      = NULL;

static const char *colors[SchemeLast][2] = {
	/*                          fg         bg          */
	[SchemeNorm]            = { "#cccccc", col_graybg  },
	[SchemeSel]             = { "#ffffff", col_frost4  },
	[SchemeSelHighlight]    = { "#ffc978", "#005577"   },
	[SchemeNormHighlight]   = { "#ffc978", "#222222"   },
	[SchemeOut]             = { "#000000", "#00ffff"   },
	[SchemeOutHighlight]    = { "#ffc978", "#00ffff"   },
	[SchemeMid]             = { "#d7d7d7", col_graybg3 },
};

/* -l option; if nonzero, dmenu uses vertical list with given number of lines */
static unsigned int lines      = 0;

/* -h option; minimum height of a menu line */
static unsigned int lineheight = 0;
static unsigned int min_lineheight = 8;

/*
 * Characters not considered part of a word while deleting words
 * for example: " /?\"&[]"
 */
static const char worddelimiters[] = " ";
